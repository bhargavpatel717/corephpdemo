   
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale==1, shrink-to-fit=no">
    <style>
    form {
        width: 35%;
        margin: 0 auto;
    }
    </style>

    <!-- js for notice period -->
    <script>
    function changeStatus() {
        var status = document.getElementById("exp");
        if (status.value == "0") {
            document.getElementById("noticep").style.visibility = "hidden";
        } else {
            document.getElementById("noticep").style.visibility = "visible";
        }
    }
    </script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Requirement Form</title>
</head>

<body>
    <?php
            if($_SERVER["REQUEST_METHOD"]=="POST"){
                include "partials/_dbconnect.php";
                include "upload.php";
                $firstname = $_POST['first_name'];
                $lastname = $_POST['last_name'];
                $email = $_POST['email'];
                $mono=$_POST['phone_number'];
                $edu=$_POST['education'];
                $age=$_POST['Age'];
                $exp=$_POST['exp'];
                $noticep=$_POST['noticep'];
                
                $sql="INSERT INTO `candidates_info` (`first_name`, `last_name`, `email`, `mobile No.`, `education`, `Age`, `Experience`, `Notice period`, `resume`, `timestamp`) VALUES ('$firstname', '$lastname', '$email', '$mono', '$edu', '$age', '$exp', '$noticep', '$filename', current_timestamp())"; 
                $result=mysqli_query($conn,$sql);

                if($result){
                    header("location: Requirement_server.php?datainsert=true");
                }
                else
                {
                    header("location: Requirement form.php?datainsert=false");
                }
            }    
            
            
            ?>
    <div class="jumbotron jumbotron-fluid">
        <div class="container text-center">
            <h1 class="display-4 "><b>Reqirement Form</b></h1>
            <p class="lead">Please fill in the form below</p>
        </div>
    </div>

    <!-- form start here -->

    <?php include "upload.php"; ?>
    <div class="container mb-4">
        <form action="Requirement form.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" required
                    aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" required
                    aria-describedby="emailHelp">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="email" name="email" required aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                    else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Mobile No.</label>
                <input type="integer" class="form-control" id="phone_number" name="phone_number" maxlength="10" required
                    aria-describedby="number">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Age</label>
                <input type="integer" class="form-control" id="Age" name="Age" maxlength="2" required
                    aria-describedby="number">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Education</label>
                <input type="text" class="form-control" id="education" name="education" required
                    aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="experience">Experience</label>
                <!-- <input type="integer" class="form-control" id=" exp" name="exp" maxlength="10" required
                    aria-describedby="number"> -->
                <select class="form-control" id="exp" name="exp" onchange="changeStatus()" required>
                    <option value="00">select</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="Notice">Notice period in month</label>
                <select class="form-control" id="noticep" name="noticep" required>
                    <option>select</option>
                    <option>0</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                </select>
            </div>
            <div class="form-group">
            <label for="resume">Upload your resume</label>
                  <p> *upload only pdf file! </p>
                  <input type="file" name="myfile"> <br>
            <br><button type="submit" name="save" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>

</html>