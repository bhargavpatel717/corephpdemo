<?php
include "partials/_dbconnect.php";
$sql = "SELECT * FROM candidates_info";
$result = mysqli_query($conn, $sql);

$files = mysqli_fetch_all($result, MYSQLI_ASSOC);
// Uploads files
if (isset($_POST['save'])) { // if save button on the form is clicked
  // name of the uploaded file
  $filename = $_FILES['myfile']['name'];

  // destination of the file on the server
  $destination = 'upload/' .$filename;

  // get the file extension
  $extension = pathinfo($filename, PATHINFO_EXTENSION);

  // the physical file on a temporary uploads directory on the server
  $file = $_FILES['myfile']['tmp_name'];
  $size = $_FILES['myfile']['size'];

  if (!in_array($extension, ['zip', 'pdf', 'docx'])) {
      echo "You file extension must be .zip, .pdf or .docx";
  } elseif ($_FILES['myfile']['size'] > 1000000) { // file shouldn't be larger than 1Megabyte
      echo "File too large!";
  } else {
      // move the uploaded (temporary) file to the specified destination
      if (move_uploaded_file($file, $destination)) {
          echo "The file ".move_uploaded_file($file, $destination)." successfully uploaded";
      } else {
          echo "Failed to upload file.";
      }
  }
}

if(isset($_GET['file_id'])){
  $id = $_GET['file_id'];

  // fetch file to download from database
  $sql = "SELECT * FROM candidates_info WHERE sno=$id";
  $result = mysqli_query($conn, $sql);

  $file = mysqli_fetch_assoc($result);
  $filepath = 'upload/' . $file['resume'];

  if (file_exists($filepath)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename=' . basename($filepath));
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize('upload/' . $file['resume']));
      readfile('upload/' . $file['resume']);
      exit;
  }

}

?>